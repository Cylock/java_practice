package OOP;


import clase.*;


public class Main {

    public static void main(String[] args) {


        // Introducere

        Masina audi = new Masina("rosu", 1, 400);

        Masina bmw = new Masina("negru", 2, 300);

        Masina  ford = new Masina (200);

        System.out.println(ford.Acceleratie(100));

        // Encapsulare

        Tracking audi_prima_serie = new Tracking("x3yzz", "20 grade nord, 30 grade est", "Proiect 2");

        System.out.println(audi_prima_serie.getSerial_number());

        audi_prima_serie.setDenumirea_locala("Proiect 3");

        System.out.println(Tracking.acces_number);

        // Inheritanta

        Ferrari nr1 = new Ferrari("Auriu", 4, 300, "488");

        // Polimorfism

        CubDeAluminiu cub1 = new CubDeAluminiu(200, 400);
        CubDeOtel cub2 = new CubDeOtel(500, 400);

        System.out.println(cub2.densitate());


    }
}