package clase;

public class CubDeAluminiu extends Blueprint
{
    public CubDeAluminiu(double paramMasaMaterial, double paramVolum)
    {
        super(paramMasaMaterial, paramVolum);
    }

    public double densitate()
    {
        double Dens = this.material  / this.volum;
        return Dens;
    }
}
